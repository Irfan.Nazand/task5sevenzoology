﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task5
{
    public class Cats : Animal, IRunner, IJumper
    {

        // Extra Variables Age and HeightofCat with getters and setters
        public int Age { get; set; }
        public int HeightOfCat { get; set; }

        //Default constructor
        public Cats() : base() { }

        // Overloaded constructor with extra variables, f.eks Age
        public Cats(string Type, bool IsVegan, int Age, int HeightOfCat) : base(Type, IsVegan)
        {
            this.Age = Age;
            this.HeightOfCat = HeightOfCat;
        }

        // Override Eat method from Animal class
        public override void Eats()
        {
            Console.WriteLine("I am " + Type + " and I love drinking milk!!");
        }

        // Run method from Interfaces class
        public void Run()
        {
            Console.WriteLine("Cats can run faster than dogs?");
        }

        // Speed method from interfaces class
        public void Speed()
        {
            Console.WriteLine("Cats can run faster than dogs with the speed of 1km/h?");
        }

        // Jump method from interfaces class
        public void Jump()
        {
            Console.WriteLine("I am a " + Type + " and I cant jumb as high as a Dog :((");
        }
    }
}
