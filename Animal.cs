﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Task5
{
    public abstract class Animal
    {
        // Variables Type and IsVegan for animals, with getter and setter
        public string Type { get; set; }
        public bool IsVegan { get; set; }

        //Default Animal constructor
        public Animal()
        {
            Type = "Default";
        }

        // overloaded Animal constructor
        public Animal(string Type, bool isVegan)
        {
            this.Type = Type;
            IsVegan = isVegan;
        }

        // Abstract eats method for animals
        public abstract void Eats();
        
        // virtual talk method for animals, can be overriden in the subclasses
        public virtual void Talk()
        {
            Console.WriteLine(Type + ": Meow Meow Meow Meow Meow Meow Meow Meow");
        }
    }
}
