﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Threading.Tasks.Sources;

namespace Task5
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Dogs> dogsList = new List<Dogs>();
            List<Cats> catsList = new List<Cats>();
           
            // Define a dog, and call methods from Dogs class
            Dogs Fluffy = new Dogs("Dog", true, 10, 1);
            Dogs Snoopy = new Dogs("Dog", false, 9, 2);
            Dogs Doopy = new Dogs("Dog", true, 4, 1);
            Dogs Woofy = new Dogs("Dog", false, 5, 6);
            Dogs Loofy = new Dogs("Dog", false, 13, 2);
            Fluffy.Eats();
            Fluffy.Talk();
            MakeAnimalsRun(Fluffy);
            MakeAnimalsJump(Fluffy);
           
            dogsList.Add(Fluffy);
            dogsList.Add(Snoopy);
            dogsList.Add(Woofy);
            dogsList.Add(Doopy);
            dogsList.Add(Loofy);
            Console.WriteLine("");

            // Define a cat and call methods from Cats class
            Cats Tom = new Cats("Cat", false, 3, 7);
            Cats Tomy = new Cats("Cat", false, 7, 2);
            Cats Tommy = new Cats("Cat", false, 5, 4);
            Cats Wommy = new Cats("Cat", false, 6, 2);
            Cats Lommy = new Cats("Cat", true, 2, 1);
            Tom.Eats();
            Tom.Talk();
            MakeAnimalsRun(Tom);
            MakeAnimalsJump(Tom);

            catsList.Add(Tom);
            catsList.Add(Tomy);
            catsList.Add(Tommy);
            catsList.Add(Wommy);
            catsList.Add(Lommy);
            Console.WriteLine("");


            IEnumerable<Dogs> dogs = from animal in dogsList
                                      where animal.Age > 3
                                      select animal;

            foreach (var animal in dogsList)
            {
                Console.WriteLine("This dog is older than 3 and is: " + animal.Age + "years old");
            }

            IEnumerable<Cats> cats = from animal in catsList
                                     where animal.Age > 2
                                     select animal;


            foreach (var animal in catsList)
            {
                Console.WriteLine("This dog is older than older tha two and is: " + animal.Age + " years old");
            }

            //From task 5, making of a collection
            #region
            /**

            List<Animal> AnimalList = new List<Animal>();

            AnimalList.Add(new Animal("Scooby", false));
            AnimalList.Add(new Animal("Tom", false));
            AnimalList.Add(new Animal("Jerry", false));
            AnimalList.Add(new Animal("Zeebo", true));
            AnimalList.Add(new Animal("Elfo", true));

            foreach (var animal in AnimalList)
            {
                Console.WriteLine("Name: {0}, Is it Vegan? {1}", animal.Name, animal.IsVegan);
            }

            **/
            #endregion
        }

        public static void MakeAnimalsRun(params IRunner[] Animals)
        {
            foreach ( IRunner Animal in Animals)
            {
                Animal.Run();
                Animal.Speed();
            }
        }

        public static void MakeAnimalsJump(params IJumper[] Animals)
        {
            foreach (IJumper Animal in Animals)
            {
                Animal.Jump();
            }
        }
    }
}
