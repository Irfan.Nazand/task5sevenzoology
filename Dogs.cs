﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task5
{
    public class Dogs : Animal, IRunner, IJumper
    {
        // Extra Variables LikesCats and HeightofDog with getters and setters
        public int Age { get; set; }
        public int HeightOfDog { get; set; }

        //Default Constructor
        public Dogs() : base() { }

        // Overloaded Constructor with its own Variables
        public Dogs(string Type, bool IsVegan, int Age, int HeightOfDog) : base(Type, IsVegan)
        {
            this.Age = Age;
            this.HeightOfDog = HeightOfDog;
        }
        
        //Eats method from Animal class
        public override void Eats()
        {
            Console.WriteLine("I am " + Type + " and I love eating chocolate, and I am " + HeightOfDog + " meter tall" );
        }

        //Talk method from Animal class, and overriden
        public override void Talk()
        {
            Console.WriteLine(Type + ": Bark Bark Bark Bark Bark!!!");
        }

        /**
         * Methods from interfaces class
         **/
        public void Run()
        {
            Console.WriteLine("I Like running more than cats");
        }

        public void Speed()
        {
            Console.WriteLine("But my speed is not faster than a cat :(("); 
        }

        public void Jump()
        {
            Console.WriteLine("But I can jump higher"); 
        }
    }
}
