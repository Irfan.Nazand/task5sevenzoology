﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task5
{

    // Runner interface with methods of run and speed
    public interface IRunner
    {
        void Run();
        void Speed();
    }

    // Interface for jumping with a jump method
    public interface IJumper
    {
        void Jump();
    }

}
